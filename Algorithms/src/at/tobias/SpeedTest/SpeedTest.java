package at.tobias.SpeedTest;

import at.tobias.Helper.DataGenerator;
import at.tobias.Main.SortEngine;
import at.tobias.SortAlgorithms.InsertionSort;
import at.tobias.SortAlgorithms.SortAlgorithm;

public class SpeedTest {
	public static void main(String[] args) {
		run(DataGenerator.generateRandomData(10, 30));
	}
	
	public static void run(int[] data) {

		SortAlgorithm algo = new InsertionSort();
		
		SortEngine se = new SortEngine();
		se.setAlgorithm(algo);
		
	}
}
