package at.tobias.Helper;

import java.util.Random;

public class DataGenerator {
	
	public static int[] generateRandomData(int size, int maxValue) {
		int[] array = new int[size];
		Random random = new Random();
		
		for (int i=0; i < size; i++) {
			array[i] = random.nextInt(maxValue);
		}
		return array;
	}
	
	public static void printData(int[] array1){
		
		for (int i : array1) {
			System.out.println(array1[i]);
		}
	}
	
}
