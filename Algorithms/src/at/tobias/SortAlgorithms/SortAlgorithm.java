package at.tobias.SortAlgorithms;

public interface SortAlgorithm {
	public int[] sort(int[] data);
}
