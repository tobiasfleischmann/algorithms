package at.tobias.SortAlgorithms;

import at.tobias.SortAlgorithms.SortAlgorithm;

public class InsertionSort implements SortAlgorithm {
	
		@Override
		public int[] sort(int[] array) {
			int a;
	        for (int i = 1; i < array.length; i++) {
	            for(int x = i ; x > 0 ; x--){
	                if(array[x] < array[x-1]){
	                    a = array[x];
	                    array[x] = array[x-1];
	                    array[x-1] = a;
	                }
	            }
	        }
	        return array;
	    }
}
