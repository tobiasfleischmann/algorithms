package at.tobias.Search;

public class NumberNotFoundException extends Exception {
	NumberNotFoundException(String reason) {
		super(reason);
	}
}
