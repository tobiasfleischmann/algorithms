package at.tobias.Search;

import at.tobias.Helper.DataGenerator;

public class BinarySearch {
	public static int search(int number) throws NumberNotFoundException {
		int[] array = DataGenerator.generateRandomData(30, 10);
		
		for (int i = 0; i < array.length; i++) {
			if(number == array[i]) {
				System.out.println("Die Zahl" + " " + number + " " + "befindet sich am Index" + " " + i + " " + "im Array");
			}
			else if(i == array.length) {
				throw new NumberNotFoundException("Die Zahl wurde nicht gefunden");
			}
		}
		
		return 0;
	}
	
	public static void main(String[] args) {
		try {
			search(7);
		} catch (NumberNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
