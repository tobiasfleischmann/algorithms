package at.tobias.Main;
import at.tobias.Helper.DataGenerator;
import at.tobias.SortAlgorithms.InsertionSort;
import at.tobias.SortAlgorithms.SortAlgorithm;
import at.tobias.SpeedTest.SpeedTest;

public class Application {

	public static void main(String[] args) {
		
		SortAlgorithm algo = new InsertionSort();
		int[] arr1 = DataGenerator.generateRandomData(30, 50);
		SortEngine se = new SortEngine();
		se.setAlgorithm(algo);
		DataGenerator.printData(se.sort(arr1));
		
		
	}
		
}
