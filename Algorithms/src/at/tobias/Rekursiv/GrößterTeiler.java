package at.tobias.Rekursiv;

public class GrößterTeiler {
	 
	public int Getggt(int a, int b) {
		if(a==b) {
			return a;
		}
		else if(a > b) {
			return Getggt(a - b, a);
		}
		else {
			return Getggt(a, b - a);
		}
	}
	
	public static void main(String[] args) {
		 GrößterTeiler r = new GrößterTeiler();
		 r.Getggt(20, 5);
	}
}
