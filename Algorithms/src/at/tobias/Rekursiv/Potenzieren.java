package at.tobias.Rekursiv;

public class Potenzieren {
	public int potenz(int n, int p) {
		if(p == 0) {
			return 1;
		}
		else {
			return potenz(n, p - 1) * n;
		}
	}
	
	public static void main(String[] args) {
		Potenzieren p = new Potenzieren();
		p.potenz(10, 1);
	}
}
