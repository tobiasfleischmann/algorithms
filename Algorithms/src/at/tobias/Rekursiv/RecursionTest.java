package at.tobias.Rekursiv;

public class RecursionTest {

	
	public static void main(String[] args) {
		
		eukRec(50, 5);
		System.out.println(factorialRec(6));
		System.out.println(potenzRec(3,3));
		printBinary(17);
		System.out.println("");
		printHex(12);
		System.out.println("");
		System.out.println(fibonacci(5));
		System.out.println(isPalindrom("otto"));
	}
	
	
	public static int addRec(int number, int cnt){
		if (cnt >=3) {
			return number;
		} else{
			return number + addRec(number, ++cnt);
		}
	}
	
	public static int eukRec(int a, int b){
		
		if (a == b){
			return a;
		}else if (a > b){
			return eukRec(a-b, b);
		
		}else{
			return eukRec(a, b-a);
		}
	}
	
	public static int factorialRec(int x){
		
		if (x == 1){
			return 1;
		}else {
			return x * factorialRec(--x);
		}
	}
	
	public static int potenzRec(int num, int p){
		
		if (p ==1){
			return num;
		}else {
			return num * potenzRec(num, --p);
		}
	}
	
	public static void printBinary(int x){
		if (x/2 ==0){
			System.out.print(x%2);
		}else{
			printBinary(x/2);
			System.out.print(x%2);
		}
	}
	
	public static void printHex (int input){
		
		if(input<16){
			System.out.print(Integer.toHexString(input));
		}else{
			printHex(input/16);
			System.out.print(Integer.toHexString(input%16));
		}
	}
	
	public static int fibonacci (int pos){
		
		if (pos == 1 || pos == 2 ) {
			return 1;
		}else{
			return fibonacci(pos-2) + fibonacci(--pos);
		}
		
	}
	
	public static boolean isPalindrom(String str){
		if(str.length()==1 || str.length()==0){
			return true;
			
		}else if(str.charAt(0) == str.charAt(str.length()-1)){
			return isPalindrom(str.substring(1,str.length()-1));
		}else{
			return false;
		}
	}
	
	
	
	
}


