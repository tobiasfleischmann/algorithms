package Test;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import at.tobias.SortAlgorithms.InsertionSort;

public class InsertionSortTest {
	private int[] data = new int[3];

	@Before
	public void setUp() throws Exception {
		data[0] = 17;
		data[1] = 10;
		data[2] = 15;
	}
	
	@Test
	public void test() {
		int[] unsorted = data.clone();
		InsertionSort is = new InsertionSort();
		int[] res = is.sort(unsorted);
		
		assertTrue(isSorted(res));
	}
	
	private boolean isSorted(int[] sorted) {
		int old = sorted[0];
		
		for (int i = 1; i < sorted.length; i++) {
			if(old < sorted[i]) {
				old = sorted[i];												
			}
			else {
				return false;
			}
		}
		return true;
	}

}
